#include <string.h>
#include <stdint.h>
#include <stdbool.h>
#include <limits.h>

#include "balloc_cfg.h"

#define BITS_PER_WORD_QTY			(sizeof(unsigned) * CHAR_BIT)
#define BUSY_MAP_SIZE_WORDS			((BALLOC_CFG_POOL_SIZE_BLOCKS) / (BITS_PER_WORD_QTY) \
											+ ((((BALLOC_CFG_POOL_SIZE_BLOCKS) % (BITS_PER_WORD_QTY)) > 0) ? 1 : 0))

typedef unsigned char Block_t[BALLOC_CFG_BLOCK_SIZE];
typedef struct
{
	unsigned bitPos;
	unsigned wordPos;
} BusyBlockMapPos_s;

static Block_t _pool[BALLOC_CFG_POOL_SIZE_BLOCKS];
static unsigned _busyMap[BUSY_MAP_SIZE_WORDS];
static unsigned long long _freeBlocksQty;

static inline unsigned _FindFreeBlockPos(BusyBlockMapPos_s * pos)
{
	unsigned freeBlockInd = UINT_MAX;

	// We use unsigned type for word because it is the fastest type for CPU
	for (unsigned word = 0; word < BUSY_MAP_SIZE_WORDS; word++)
	{
		if (_busyMap[word] != UINT_MAX)		// Coarse search by word
		{
			// Here we must analyze only available bits mapped on blocks in the word,
			// otherwise we will scan inexistent blocks and we may spoil the memory
			const unsigned blocksLeftQty = BALLOC_CFG_POOL_SIZE_BLOCKS - word * BITS_PER_WORD_QTY;
			const unsigned curWordBitsAvailableQty = blocksLeftQty > BITS_PER_WORD_QTY ? BITS_PER_WORD_QTY : blocksLeftQty;

			for (unsigned bit = 0; bit < curWordBitsAvailableQty; bit++)
			{
				if ((_busyMap[word] & (1U << bit)) == 0)	// Fine search by bit in the word
				{
					freeBlockInd = word * BITS_PER_WORD_QTY + bit;
					pos->bitPos = bit;
					pos->wordPos = word;
					break;
				}
			}
		}

		if (freeBlockInd != UINT_MAX)
		{
			break;
		}
	}
	return freeBlockInd;
}

static inline void _SetBlockState(const BusyBlockMapPos_s * busyMapPos, bool busy)
{
	_busyMap[busyMapPos->wordPos] = busy
			? (_busyMap[busyMapPos->wordPos] | (1U << busyMapPos->bitPos))
			: (_busyMap[busyMapPos->wordPos] & ~(1U << busyMapPos->bitPos));

}

static inline void _CalcBusyMapBlockPos(unsigned blockInd, BusyBlockMapPos_s * busyMapPos)
{
	busyMapPos->wordPos = blockInd / BITS_PER_WORD_QTY;
	busyMapPos->bitPos = blockInd % BITS_PER_WORD_QTY;
}

void BALLOC_Init(void)
{
	memset(_busyMap, 0, sizeof(_busyMap));			// Set all blocks free
	_freeBlocksQty = BALLOC_CFG_POOL_SIZE_BLOCKS;
}

void * BALLOC_AllocateBlock(BALLOC_CFG_SYS_TICKS_TYPE ticksToWaitQty)
{
	unsigned freeBlockInd;
	bool allocated;

	BALLOC_CFG_SYS_TICKS_TYPE startTicksQty = BALLOC_CFG_GET_TICKS();

	while(1)
	{
		BALLOC_CRITICAL_SECTION_START();

		BALLOC_CFG_SYS_TICKS_TYPE curTicksQty;

		allocated = (_freeBlocksQty > 0);			// Free block exists

		if (allocated)
		{
			BusyBlockMapPos_s busyMapPos = {0};

			freeBlockInd = _FindFreeBlockPos(&busyMapPos);
			_freeBlocksQty--;
			_SetBlockState(&busyMapPos, true);		// We don't need to get time here
		}											// as we already got free block
		else
		{
			curTicksQty = BALLOC_CFG_GET_TICKS();
		}

		const bool exit = allocated || (ticksToWaitQty == 0)
				|| ((curTicksQty - startTicksQty) >= ticksToWaitQty);

		BALLOC_CRITICAL_SECTION_END();	// Give possibility to other tasks or ISR to preempt

		if (exit)
		{
			break;
		}
		else
		{
			BALLOC_CFG_TASK_YIELD();		// We can now give control to another task
		}
	}

	return allocated ? &_pool[freeBlockInd] : NULL;
}

void BALLOC_DeallocateBlock(const void * ptr)
{
	const unsigned blockInd = ((uintptr_t) ptr - (uintptr_t) _pool) / BALLOC_CFG_BLOCK_SIZE;
	BusyBlockMapPos_s busyMapPos;

	BALLOC_CRITICAL_SECTION_START();

	_CalcBusyMapBlockPos(blockInd, &busyMapPos);
	_SetBlockState(&busyMapPos, false);
	_freeBlocksQty++;

	BALLOC_CRITICAL_SECTION_END();
}
