#ifndef BALLOC_H_
#define BALLOC_H_

#include "balloc_cfg.h"

void BALLOC_Init(void);
void * BALLOC_AllocateBlock(BALLOC_CFG_SYS_TICKS_TYPE ticksToWaitQty);
void BALLOC_DeallocateBlock(const void * ptr);


#endif /* BALLOC_H_ */
