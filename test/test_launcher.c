#include <unity/unity.h>
#include <balloc/balloc.h>

void setUp()
{
	BALLOC_Init();
}

void tearDown()
{

}

extern void TEST_ShouldAllocateMemCorrectly(void);
extern void TEST_ShouldFreeMemCorrectly(void);
extern void TEST_ShouldAllocateCorrectBlocksQty(void);

int main()
{
	UNITY_BEGIN();

	RUN_TEST(TEST_ShouldAllocateMemCorrectly);
	RUN_TEST(TEST_ShouldFreeMemCorrectly);
	RUN_TEST(TEST_ShouldAllocateCorrectBlocksQty);

	UNITY_END();
	return 0;
}
