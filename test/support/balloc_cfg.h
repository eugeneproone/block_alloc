#ifndef BALLOC_CFG_H_
#define BALLOC_CFG_H_

#define BALLOC_CFG_BLOCK_SIZE 				100
#define BALLOC_CFG_POOL_SIZE_BLOCKS			1000
#define BALLOC_CFG_SYS_TICKS_TYPE			unsigned long
#define BALLOC_CFG_GET_TICKS()				0
#define BALLOC_CFG_TASK_YIELD()				((void) 0)

#define BALLOC_CRITICAL_SECTION_START(...)
#define BALLOC_CRITICAL_SECTION_END(...)


#endif /* BALLOC_CFG_H_ */
