#include <stdint.h>
#include <string.h>

#include <unity/unity.h>

#include <balloc/balloc.h>


#include "balloc_cfg.h"



void TEST_ShouldAllocateMemCorrectly(void)
{
	void * ptrs[BALLOC_CFG_POOL_SIZE_BLOCKS];

	for (unsigned b = 0; b < BALLOC_CFG_POOL_SIZE_BLOCKS; b++)
	{
		ptrs[b] = BALLOC_AllocateBlock(0);
		TEST_ASSERT_NOT_NULL(ptrs[b]);
		memset(ptrs[b], (int) b, BALLOC_CFG_BLOCK_SIZE);
	}

	TEST_ASSERT_NULL(BALLOC_AllocateBlock(0));

	for (unsigned b = 0; b < BALLOC_CFG_POOL_SIZE_BLOCKS; b++)
	{
		TEST_ASSERT_EACH_EQUAL_UINT8(b, ptrs[b], BALLOC_CFG_BLOCK_SIZE);
	}
}

void TEST_ShouldFreeMemCorrectly(void)
{
	const void * const ptr1 = BALLOC_AllocateBlock(0);
	TEST_ASSERT_NOT_NULL(ptr1);

	BALLOC_DeallocateBlock(ptr1);

	const void * const ptr2 = BALLOC_AllocateBlock(0);
	TEST_ASSERT_EQUAL(ptr1, ptr2);
}

void TEST_ShouldAllocateCorrectBlocksQty(void)
{
	for (unsigned b = 0; b < BALLOC_CFG_POOL_SIZE_BLOCKS; b++)
	{
		TEST_ASSERT_NOT_NULL(BALLOC_AllocateBlock(0));
	}
	TEST_ASSERT_NULL(BALLOC_AllocateBlock(0));
}
